plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("com.google.dagger.hilt.android")
    id("kotlin-parcelize")
    kotlin("kapt")
}

android {
    namespace = "com.example.ca_mybank_clone"
    compileSdk = ConfigData.compileSdkVersion

    defaultConfig {
        applicationId = ConfigData.appId
        minSdk = ConfigData.minSdkVersion
        targetSdk = ConfigData.targetSdkVersion
        versionCode = ConfigData.versionCode
        versionName = ConfigData.versionName
        buildConfigField(
            "String",
            "BASE_URL",
            "\"https://cdf-test-mobile-default-rtdb.europe-west1.firebasedatabase.app\""
        )
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        compose = true
        buildConfig = true
    }

    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {

    implementation(Deps.coreKtx)
    implementation(Deps.lifecycle)
    implementation(Deps.composeLifecycle)
    implementation(Deps.compose)
    implementation(platform(Deps.bom))
    implementation(Deps.composeUi)
    implementation(Deps.composeGraphics)
    implementation(Deps.toolingPreview)
    implementation(Deps.material3)
    debugImplementation(Deps.uiTooling)
    debugImplementation(Deps.uiTestManifest)


   // unit test
    testImplementation(Deps.mockServer)
    testImplementation(Deps.truth)
    testImplementation(Deps.mockk)
    testImplementation(Deps.junit)
    testImplementation(Deps.coroutinesTest)
    testImplementation ("androidx.arch.core:core-testing:2.2.0")
    testImplementation ("com.android.support.test:runner:1.0.2")

    //ui testing
    androidTestImplementation(Deps.junitInstrumental)
    androidTestImplementation(Deps.espresso)
    androidTestImplementation(platform(Deps.bom))
    androidTestImplementation(Deps.junitComposeUi)
    androidTestImplementation(Deps.coroutinesTest)




    //Retrofit
    implementation(Deps.retrofit)
    implementation(Deps.gson)
    implementation(Deps.logginfInterceptor)


    //hilt for DI
    implementation(Deps.hilt)
    implementation(Deps.hiltNavigation)
    kapt(Deps.hiltCompiler)


    //coroutines
    implementation(Deps.coroutines)


    //navigation
    implementation(Deps.navigation)
    implementation(Deps.navigationAnnimation)

}