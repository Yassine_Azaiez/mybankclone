package com.example.ca_mybank_clone.data.repository

import com.example.ca_mybank_clone.core.BANK_MOCK_JSON_FILE_NAME
import com.example.ca_mybank_clone.core.extenssions.fromJsonToObjectType
import com.example.ca_mybank_clone.core.networkUtils.ApiResult
import com.example.ca_mybank_clone.data.dataSource.MyBankDataSource
import com.example.ca_mybank_clone.data.response.Bank
import com.example.ca_mybank_clone.domain.repository.MyBankRepository
import com.example.ca_mybank_clone.helpers.getJson
import com.google.common.truth.Truth
import com.google.gson.Gson
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.TestDispatcher
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestWatcher
import org.junit.runner.Description


@OptIn(ExperimentalCoroutinesApi::class)
class MainDispatcherRule constructor(
    val testDispatcher: TestDispatcher = UnconfinedTestDispatcher(),
) : TestWatcher() {

    override fun starting(description: Description) {
        Dispatchers.setMain(testDispatcher)
    }

    override fun finished(description: Description) {
        Dispatchers.resetMain()
    }
}

@OptIn(ExperimentalCoroutinesApi::class)
class MyBankRepositoryImpTest {
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @MockK
    private lateinit var myBankDataSource: MyBankDataSource

    private lateinit var myBankRepo: MyBankRepository

    private val gson = Gson()


    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        myBankRepo = MyBankRepositoryImp(
            myBankDataSource, mainDispatcherRule.testDispatcher
        )

    }


    @Test
    fun `assert that getAccountsList returns success result`() = runTest {
        val response = gson.fromJsonToObjectType<List<Bank>>(getJson(BANK_MOCK_JSON_FILE_NAME))
        coEvery {
            myBankDataSource.getAccountsList()
        } returns ApiResult.Success(response)


        val result = myBankRepo.getAccountsLIst().toList().last()


        coVerify(exactly = 1) {  myBankDataSource.getAccountsList() }

        Truth.assertThat((result as ApiResult.Success).data.size).isEqualTo(4)


    }



@Test
fun `assert that getAccountsList returns error result`() = runTest {

    coEvery {
        myBankDataSource.getAccountsList()
    } returns ApiResult.Error.ServerError(ApiResult.Error.ServerError.Reason.UNKNOWN)


    val result = myBankRepo.getAccountsLIst().toList().last()


    coVerify(exactly = 1) {  myBankDataSource.getAccountsList() }

    Truth.assertThat((result as ApiResult.Error.ServerError).reason).isEqualTo(ApiResult.Error.ServerError.Reason.UNKNOWN)


}
}