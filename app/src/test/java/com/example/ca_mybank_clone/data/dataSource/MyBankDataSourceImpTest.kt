package com.example.ca_mybank_clone.data.dataSource

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.ca_mybank_clone.core.BANK_MOCK_JSON_FILE_NAME
import com.example.ca_mybank_clone.core.networkUtils.ApiResult
import com.example.ca_mybank_clone.data.remoteApi.MyBankApi
import com.example.ca_mybank_clone.helpers.getJson
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import okhttp3.HttpUrl
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@OptIn(ExperimentalCoroutinesApi::class)
class MyBankDataSourceImpTest {

    @get:Rule
    val executorRule = InstantTaskExecutorRule()

    private val mockServer: MockWebServer = MockWebServer()
    private lateinit var dataSourceImp: MyBankDataSource
    private lateinit var myBankApi: MyBankApi

    private lateinit var baseUrl: HttpUrl

    @Before
    fun setUp() {
        mockServer.start()
        baseUrl = mockServer.url("/")
        setUpMyBankService()
        dataSourceImp = MyBankDataSourceImp(myBankApi)
    }


    @After
    fun tearDown() {
        mockServer.shutdown()
    }


    @Test
    fun `Assert getAccountsList returns valid api response`() = runTest {
        mockServer.enqueue(MockResponse().setResponseCode(200).setBody(getJson(BANK_MOCK_JSON_FILE_NAME)))
        val list = when (val result = dataSourceImp.getAccountsList()) {
            is ApiResult.Success -> result.data
            else -> emptyList()
        }

        Truth.assertThat(list?.size).isEqualTo(4)
        Truth.assertThat(list?.first()?.name).isEqualTo("CA Languedoc")
    }

    @Test
    fun `Assert getAccountsList returns error when it fails`() = runTest {
        mockServer.enqueue(MockResponse().setResponseCode(400))
        val result = dataSourceImp.getAccountsList()


        Truth.assertThat(result).isInstanceOf(ApiResult.Error.ServerError::class.java)
    }


    private fun setUpMyBankService() {
        myBankApi = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(mockServer.url(baseUrl.encodedPath))
            .build()
            .create(MyBankApi::class.java)
    }


}