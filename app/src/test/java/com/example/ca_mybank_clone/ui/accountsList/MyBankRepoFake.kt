package com.example.ca_mybank_clone.ui.accountsList

import com.example.ca_mybank_clone.core.BANK_MOCK_JSON_FILE_NAME
import com.example.ca_mybank_clone.core.extenssions.fromJsonToObjectType
import com.example.ca_mybank_clone.core.networkUtils.ApiResult
import com.example.ca_mybank_clone.data.response.Bank
import com.example.ca_mybank_clone.domain.mapper.toBankModel
import com.example.ca_mybank_clone.domain.models.BankModel
import com.example.ca_mybank_clone.domain.repository.MyBankRepository
import com.example.ca_mybank_clone.helpers.getJson
import com.google.gson.Gson
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class MyBankRepoFake : MyBankRepository {

    private val gson = Gson()
    private val listOfBanks =
        gson.fromJsonToObjectType<List<Bank>>(getJson(BANK_MOCK_JSON_FILE_NAME))
            .map { it.toBankModel() }
    override fun getAccountsLIst(): Flow<ApiResult<List<BankModel>>> = flow {

        emit(ApiResult.Success(listOfBanks))
    }
}