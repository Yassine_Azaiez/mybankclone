package com.example.ca_mybank_clone.ui.accountsList

import com.example.ca_mybank_clone.data.repository.MainDispatcherRule
import com.example.ca_mybank_clone.data.response.AccountType
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class AccountListViewModelTest {


    @get:Rule
    val mainDispatcherRule = MainDispatcherRule(UnconfinedTestDispatcher())


    private lateinit var myBankRepo: MyBankRepoFake

    private lateinit var viewModel: AccountListViewModel


    @Before
    fun setup() {

        myBankRepo = MyBankRepoFake()
        viewModel = AccountListViewModel(myBankRepo)
    }


    @Test
    fun `assert that getTAccountsLIst returns success when api call is successful`() = runTest {


        viewModel.getAccountsList()

        val expected = (viewModel.accountList.first())

        Truth.assertThat(expected[AccountType.CA]?.size).isEqualTo(2)


    }


}