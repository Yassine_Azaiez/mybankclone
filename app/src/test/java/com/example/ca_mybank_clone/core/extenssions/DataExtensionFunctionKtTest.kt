package com.example.ca_mybank_clone.core.extenssions

import com.example.ca_mybank_clone.data.response.Operation
import com.google.common.truth.Truth
import com.google.common.truth.Truth.assertThat
import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.google.gson.JsonSyntaxException
import org.junit.Test


class DataExtensionFunctionKtTest {

    @Test
    fun `Assert that when passing a correct json returns the desired object`() {

        val operationObjectJson =
            """
                {
                                    "id": "2",
                                    "title": "Prélèvement Netflix",
                                    "amount": "-15,99",
                                    "category": "leisure",
                                   "date": "1644870724"
                                }
            """.trimIndent()

        val operationResource = Operation(
            id = "2",
            title = "Prélèvement Netflix",
            amount = "-15,99",
            category = "leisure",
            date = "1644870724"
        )

        val expectedResult =
            operationObjectJson.fromJsonToObject(Operation::class.java)

        assertThat(expectedResult).isEqualTo(operationResource)

    }

    @Test
    fun `Assert that when passing a corrupted json returns null`() {

        val operationObjectJson =
            """
                {
                                    "id": "2",
                                    "title": "Prélèvement Netflix",
                                    "amount": "-15,99",
                                    "category": "leisure"
                                   "date": 
                                }
            """.trimIndent()


        val expectedResult =
            operationObjectJson.fromJsonToObject(Operation::class.java)

        assertThat(expectedResult).isNull()

    }


    @Test(expected = JsonSyntaxException::class)
    fun `Assert that gson build  returns error  when passing a corrupted json`() {
        val gson = Gson()

        val operationObjectJson =
            """
                {
                                    "id": "2",
                                    "title": "Prélèvement Netflix",
                                    "amount": "-15,99",
                                    "category": "leisure"
                                   "date": 
                                
            """.trimIndent()

        gson.fromJsonToObjectType<Operation>(operationObjectJson)

    }

    @Test
    fun `Assert that gson build  returns success  when passing a correct json`() {
        val gson = Gson()
        val operationObjectJson =
            """
                {
                                    "id": "2",
                                    "title": "Prélèvement Netflix",
                                    "amount": "-15,99",
                                    "category": "leisure",
                                   "date": "1644870724"
                   }             
            """.trimIndent()

        val operationResource = Operation(
            id = "2",
            title = "Prélèvement Netflix",
            amount = "-15,99",
            category = "leisure",
            date = "1644870724"
        )

        val expectedResult =
            gson.fromJsonToObjectType<Operation>(operationObjectJson)

        assertThat(expectedResult).isEqualTo(operationResource)

    }




    @Test(expected = JsonParseException::class)
    fun `Assert that gson build  returns error  when passing a invalid json`() {
        val gson = Gson()
        val operationObjectJson =
            """
                {
                                    "id": "2",
                                    "title": Prélèvement Netflix,
                                    "amount": "-15,99",
                                    "category": "leisure",
                                   "date": 1644870724"
                     }
            """.trimIndent()


        gson.fromJsonToObjectType<Operation>(operationObjectJson)

    }



    @Test()
    fun `Assert that passing correct timeStamp returns valid date`() {
      //Arrange
        val timeStamp  = "1644784369"

        //act
        val expected = timeStamp.timestampToDate()

        //Assert
        Truth.assertThat(expected).isEqualTo("13/02/2022")

    }

}