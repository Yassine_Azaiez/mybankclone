package com.example.ca_mybank_clone.domain.mapper

import com.example.ca_mybank_clone.data.response.Account
import com.example.ca_mybank_clone.data.response.AccountType
import com.example.ca_mybank_clone.data.response.Bank
import com.example.ca_mybank_clone.data.response.Operation
import com.google.common.truth.Truth
import org.junit.Test


class MyBankMapperKtTest {


    @Test
    fun `Assert that maping to bank model works correctly`() {
        val bankResponse = Bank(
            "CA",
            1,
            emptyList()
        )

        val expected = bankResponse.toBankModel()

        Truth.assertThat(expected.name).isEqualTo("CA")
        Truth.assertThat(expected.type).isEqualTo(AccountType.CA)
        Truth.assertThat(expected.isCaAccount).isEqualTo(true)
        Truth.assertThat(expected.accounts.size).isEqualTo(0)

    }


    @Test
    fun `Assert that maping to account model works correctly`() {
        val accountResponse = Account(
            0, "1230", "yassine", 1, "12345678", "test", "0", 123.2, emptyList()
        )

        val expected = accountResponse.toAccountModel()

        Truth.assertThat(expected.balance).isEqualTo(123.2)
        Truth.assertThat(expected.holder).isEqualTo("yassine")
        Truth.assertThat(expected.operations.size).isEqualTo(0)


    }


    @Test
    fun `Assert that maping to operation model works correctly`() {
        val operationResponse = Operation(
            null,"test1","123.4","virement","1644784369"
        )

        val expected = operationResponse.toOperationModel()

        Truth.assertThat(expected.id).isEqualTo("")
        Truth.assertThat(expected.amount).isEqualTo("123.4")
        Truth.assertThat(expected.date).isEqualTo("13/02/2022")
        Truth.assertThat(expected.title).isEqualTo("test1")

    }


}