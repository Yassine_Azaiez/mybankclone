package com.example.ca_mybank_clone.core.di

import android.content.Context
import com.example.ca_mybank_clone.core.networkUtils.MyBankApiClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {


    @Singleton
    @Provides
    fun provideHttpClient(cache: Cache): OkHttpClient =
        MyBankApiClient.providesOkHttpClient(cache)

    @Singleton
    @Provides
    fun provideCache(@ApplicationContext context: Context) =
        MyBankApiClient.providesOkHttpCache(context)

    @Singleton
    @Provides
    fun provideRetrofit(httpClient: OkHttpClient) =
        MyBankApiClient.providesRetrofitInstance(httpClient)


}