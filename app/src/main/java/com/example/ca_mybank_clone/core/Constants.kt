package com.example.ca_mybank_clone.core

const val GET_ACCOUNTS_LIST = "/banks.json"
const val DATE_FORMAT = "dd/MM/yyyy"
const val ACCOUNT = "account"
const val BANK_MOCK_JSON_FILE_NAME = "banks.json"
