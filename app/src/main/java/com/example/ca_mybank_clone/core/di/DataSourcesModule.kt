package com.example.ca_mybank_clone.core.di

import com.example.ca_mybank_clone.data.dataSource.MyBankDataSource
import com.example.ca_mybank_clone.data.dataSource.MyBankDataSourceImp
import com.example.ca_mybank_clone.data.remoteApi.MyBankApi
import com.example.ca_mybank_clone.data.repository.MyBankRepositoryImp
import com.example.ca_mybank_clone.domain.repository.MyBankRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher

@Module
@InstallIn(SingletonComponent::class)
object DataSourcesModule {

    @Provides
    fun provideAppRemoteDataSource(
        apiService: MyBankApi
    ): MyBankDataSource = MyBankDataSourceImp(apiService)


    @Provides
    fun provideAppRepository(
        dataSource: MyBankDataSource,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ): MyBankRepository = MyBankRepositoryImp(dataSource, ioDispatcher)

}