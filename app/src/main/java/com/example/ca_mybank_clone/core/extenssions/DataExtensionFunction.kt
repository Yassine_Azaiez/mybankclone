package com.example.ca_mybank_clone.core.extenssions

import com.example.ca_mybank_clone.core.DATE_FORMAT
import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.google.gson.reflect.TypeToken
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


val String.Companion.Empty
    inline get() = ""


fun String.timestampToDate(): String {

    return try {
        val date = Date(this.toLong() * 1000L)
        val dateFormat = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())
        dateFormat.format(date)
    } catch (e: NumberFormatException) {
        e.printStackTrace()
        ""
    }

}



/**
 * function that converts data from json to object
 * @param classMapper the class of T.
 * @param <T> the type of the desired object.
 * @return an object of type T from the string.
 */
fun <T> String.fromJsonToObject(classMapper: Class<T>): T? {
    return try {
        Gson().fromJson(this, classMapper)
    } catch (e: JsonParseException) {
        null
    }
}


/**
 * inline function to convert json string to a TypeToken generic type
 * can be used in two ways like in the example below
 * val case1 = Gson().fromJsonToObjectType<SharedResponse<List<FuelPriceEntity>>>(result.data.toString())
 * val case2 : SharedResponse<List<FuelPriceEntity>> = Gson().fromJsonToObjectType(result.data.toString())
 */
inline fun <reified T> Gson.fromJsonToObjectType(json: String): T =
    fromJson(json, object : TypeToken<T>() {}.type)
