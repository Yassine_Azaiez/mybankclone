package com.example.ca_mybank_clone.ui.accountsList.components

import android.content.res.Configuration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.ca_mybank_clone.core.extenssions.noRippleClickable
import com.example.ca_mybank_clone.ui.theme.CA_MyBank_CloneTheme

@Composable
fun ExpandableItemContent(
    modifier: Modifier,
    title: String,
    balance: String,
    onItemClicked: () -> Unit
) {
    Column(modifier) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .noRippleClickable {
                    onItemClicked()
                },
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = title,
                style = MaterialTheme.typography.labelMedium
            )
            Row(horizontalArrangement = Arrangement.spacedBy(10.dp)) {
                Text(
                    text = balance,
                    style = MaterialTheme.typography.labelMedium,
                    color= Color.LightGray
                )
                Icon(
                    imageVector = Icons.Default.KeyboardArrowRight,
                    contentDescription = "arrow right",
                    modifier = Modifier.align(Alignment.CenterVertically)
                )
            }
        }
        Divider()
    }

}

@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun ExpandableItemContentPreview() {
    CA_MyBank_CloneTheme(dynamicColor = false) {
        ExpandableItemContent(
            modifier = Modifier, "ca account", "1234.5"
        ) {


        }
    }
}