package com.example.ca_mybank_clone.ui.screenDestination

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import com.example.ca_mybank_clone.core.ACCOUNT
import com.example.ca_mybank_clone.domain.models.AccountModel
import com.example.ca_mybank_clone.ui.accountdetails.accountsDetailsRoute
import com.example.ca_mybank_clone.ui.accountsList.accountsListRoute
import com.example.ca_mybank_clone.ui.screenDestination.bottomNavigation.Screen
import com.example.ca_mybank_clone.ui.splashScreen.splashScreenRoute
import com.google.accompanist.navigation.animation.AnimatedNavHost

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun MyBankCloneAppNavGraph(navController: NavHostController, modifier: Modifier) {

    val accountDetails =
        navController.previousBackStackEntry?.savedStateHandle?.get<AccountModel>(ACCOUNT)
            ?: AccountModel("", 0.0, emptyList())

    AnimatedNavHost(navController = navController, startDestination = Screen.Splash.route) {
        splashScreenRoute {
            navController.navigate(Screen.AccountsList.route) {
                popUpTo(Screen.Splash.route) { inclusive = true }
            }
        }
        accountsListRoute(modifier) {
            navController.currentBackStackEntry?.savedStateHandle?.set(
                key = ACCOUNT,
                value = it
            )
            navController.navigate(Screen.AccountDetails.route)
        }

        accountsDetailsRoute(accountDetails) {
            navController.popBackStack()
        }

    }
}