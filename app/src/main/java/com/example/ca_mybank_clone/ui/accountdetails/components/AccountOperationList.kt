package com.example.ca_mybank_clone.ui.accountdetails.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.example.ca_mybank_clone.domain.models.OperationModel
import com.example.ca_mybank_clone.ui.theme.Dimensions

@Composable
fun AccountOperationList(
    operations: List<OperationModel>
) {
    LazyColumn(
        verticalArrangement = Arrangement.spacedBy(Dimensions.paddingValues)
    ) {
        items(operations) { operation ->
            OperationItem(
                modifier = Modifier
                    .padding(start = Dimensions.paddingValues, end = Dimensions.largePadding)
                    .fillMaxSize(),
                operation
            )
        }
    }
}