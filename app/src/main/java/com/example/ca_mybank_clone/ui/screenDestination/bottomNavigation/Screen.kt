package com.example.ca_mybank_clone.ui.screenDestination.bottomNavigation

sealed class Screen (val route : String){
    object Splash : Screen("splashScreen")
    object AccountsList : Screen("accountsList")
    object AccountDetails : Screen("accountsList/details")

}
