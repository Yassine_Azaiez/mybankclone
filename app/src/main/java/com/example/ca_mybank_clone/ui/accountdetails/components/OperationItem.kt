package com.example.ca_mybank_clone.ui.accountdetails.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import com.example.ca_mybank_clone.R
import com.example.ca_mybank_clone.domain.models.OperationModel
import com.example.ca_mybank_clone.ui.theme.Dimensions

@Composable
fun OperationItem(modifier: Modifier, operation: OperationModel) {
    Row(
        modifier = modifier.padding(vertical = Dimensions.smallPadding),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Column(
            verticalArrangement = Arrangement.spacedBy(Dimensions.mediumPadding),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(text = operation.title)
            Text(
                text = operation.date,
                textAlign = TextAlign.Center
            )
        }
        Text(
            text = stringResource(id = R.string.balanceInquiry, operation.amount),
            style = MaterialTheme.typography.labelMedium,
            color = Color.LightGray
        )
    }
    Divider()
}