package com.example.ca_mybank_clone.ui

import android.annotation.SuppressLint
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.LargeTopAppBar
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.stringResource
import androidx.navigation.compose.currentBackStackEntryAsState
import com.example.ca_mybank_clone.R
import com.example.ca_mybank_clone.ui.screenDestination.bottomNavigation.bottomNavigationBarItemsList
import com.example.ca_mybank_clone.ui.screenDestination.MyBankCloneAppNavGraph
import com.example.ca_mybank_clone.ui.screenDestination.bottomNavigation.NewsBottomNavigation
import com.google.accompanist.navigation.animation.rememberAnimatedNavController

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalAnimationApi::class, ExperimentalMaterial3Api::class)
@Composable
fun MyBankCloneApp() {


    val navController = rememberAnimatedNavController()
    val backStackEntry = navController.currentBackStackEntryAsState()
    val showAppBars =
        (backStackEntry.value?.destination?.route in bottomNavigationBarItemsList.map { it.itemRoute })

    val scrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior()
    Scaffold(
        modifier = Modifier
            .fillMaxSize()
            .nestedScroll(scrollBehavior.nestedScrollConnection),
        bottomBar = {
      if(showAppBars) NewsBottomNavigation(bottomNavigationBarItemsList)

    }, topBar = {
            if(showAppBars) LargeTopAppBar(
            title = {
                Text(text = stringResource(id = R.string.mes_comptes))
            },scrollBehavior = scrollBehavior)

    }

    ) {
        MyBankCloneAppNavGraph(navController = navController,Modifier.padding(it))
    }

}
