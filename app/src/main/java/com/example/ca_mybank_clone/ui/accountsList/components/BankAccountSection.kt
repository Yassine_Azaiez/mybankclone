package com.example.ca_mybank_clone.ui.accountsList.components

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.ca_mybank_clone.data.response.AccountType
import com.example.ca_mybank_clone.domain.models.AccountModel
import com.example.ca_mybank_clone.domain.models.BankModel
import com.example.ca_mybank_clone.ui.theme.CA_MyBank_CloneTheme
import com.example.ca_mybank_clone.ui.theme.Dimensions.paddingValues
import com.example.ca_mybank_clone.ui.theme.titleBackgroundColor
import com.example.ca_mybank_clone.ui.theme.titleColor


@Composable
fun BankAccountSection(
    modifier: Modifier,
    section: Map<AccountType, List<BankModel>>,
    onAccountClicked: (AccountModel) -> Unit
    )  {

    LazyColumn(
        modifier = modifier
            .fillMaxSize()
    ) {
        items(section.keys.size) {
            val bankName = section.keys.elementAt(it)
            Column(Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.spacedBy(10.dp)) {
                Text(
                    text = bankName.typeName,
                    style= MaterialTheme.typography.labelLarge,
                    color = titleColor,
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(titleBackgroundColor)
                        .padding(paddingValues)

                )
                section[bankName]?.sortedBy { bank -> bank.name }
                    ?.forEach { bank ->

                        ExpandableItem(
                            itemList = bank.accounts,
                            headerText = bank.name
                        ) { account ->
                            onAccountClicked(account)
                        }
                    }
            }
        }
    }

}


@Preview
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun BankAccountSectionPreview() {
    CA_MyBank_CloneTheme(dynamicColor = false) {
        BankAccountSection(
            modifier = Modifier, hashMapOf(
                AccountType.CA to listOf(
                    BankModel(
                        "ca", true,
                        emptyList(), AccountType.CA
                    )
                )
            )
        ) {


        }
    }
}



