package com.example.ca_mybank_clone.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontLoadingStrategy
import androidx.compose.ui.text.font.FontWeight
import com.example.ca_mybank_clone.R

/**
 * https://fonts.google.com/specimen/Montserrat
 */
private val Montserrat = FontFamily(
    Font(R.font.montserrat_regular, loadingStrategy = FontLoadingStrategy.OptionalLocal),
    Font(R.font.montserrat_medium, FontWeight.W500, loadingStrategy = FontLoadingStrategy.OptionalLocal),
    Font(R.font.montserrat_semibold, FontWeight.W600, loadingStrategy = FontLoadingStrategy.OptionalLocal)
)

/**
 * https://fonts.google.com/specimen/Domine
 */
private val Domine = FontFamily(
    Font(R.font.domine_regular, loadingStrategy = FontLoadingStrategy.OptionalLocal),
    Font(R.font.domine_bold, FontWeight.Bold, loadingStrategy = FontLoadingStrategy.OptionalLocal)
)

val Typography: Typography = with(Typography()) {
    copy(
        displayLarge = displayLarge.copy(fontFamily = Montserrat),
        displayMedium = displayMedium.copy(fontFamily = Montserrat),
        displaySmall = displaySmall.copy(fontFamily = Montserrat),
        headlineLarge = headlineLarge.copy(fontFamily = Montserrat),
        headlineMedium = headlineMedium.copy(fontFamily = Montserrat),
        headlineSmall = headlineSmall.copy(fontFamily = Montserrat),
        titleLarge = titleLarge.copy(fontFamily = Montserrat),
        titleMedium = titleMedium.copy(fontFamily = Montserrat),
        titleSmall = titleSmall.copy(fontFamily = Montserrat),
        bodyLarge = bodyLarge.copy(fontFamily = Domine),
        bodyMedium = bodyMedium.copy(fontFamily = Montserrat),
        bodySmall = bodySmall.copy(fontFamily = Montserrat),
        labelLarge = labelLarge.copy(fontFamily = Montserrat),
        labelMedium = labelMedium.copy(fontFamily = Montserrat),
        labelSmall = labelSmall.copy(fontFamily = Montserrat),
    )
}