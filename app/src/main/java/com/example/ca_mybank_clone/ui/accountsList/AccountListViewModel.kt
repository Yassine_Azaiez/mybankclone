package com.example.ca_mybank_clone.ui.accountsList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ca_mybank_clone.core.networkUtils.ApiResult
import com.example.ca_mybank_clone.data.response.AccountType
import com.example.ca_mybank_clone.domain.models.BankModel
import com.example.ca_mybank_clone.domain.repository.MyBankRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AccountListViewModel @Inject constructor(
    private val myBankRepo : MyBankRepository
) : ViewModel(){

    private val _accountList: MutableStateFlow<Map<AccountType,List<BankModel>>> = MutableStateFlow(emptyMap())
    val accountList: MutableStateFlow<Map<AccountType, List<BankModel>>> get() = _accountList


     init {
         getAccountsList()
     }


    fun getAccountsList() {
        viewModelScope.launch {
            myBankRepo.getAccountsLIst().collect {
                 when(it) {
                     is ApiResult.Success ->
                         _accountList.value =sortAccountListResult(it.data)
                     else -> {

                     }
                 }
            }
        }
    }

    private fun sortAccountListResult(list : List<BankModel>) =  list.sortedBy { it.type.ordinal}.groupBy { it.type}

}