package com.example.ca_mybank_clone.ui

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyBankApp :Application()
