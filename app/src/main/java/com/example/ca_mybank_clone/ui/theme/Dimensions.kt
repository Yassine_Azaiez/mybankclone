package com.example.ca_mybank_clone.ui.theme

import androidx.compose.ui.unit.dp


object Dimensions {

    val extraSmallPadding = 5.dp
    val smallPadding = 6.dp
    val mediumPadding = 8.dp
    val paddingValues = 16.dp
    val largePadding = 32.dp
    object BottomBar {
        val BottomNavHeight = 64.dp
        val iconSize = 20.dp
    }
}