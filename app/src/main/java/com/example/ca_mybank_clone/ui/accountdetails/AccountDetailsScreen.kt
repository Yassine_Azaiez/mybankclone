package com.example.ca_mybank_clone.ui.accountdetails

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.ca_mybank_clone.R
import com.example.ca_mybank_clone.core.extenssions.noRippleClickable
import com.example.ca_mybank_clone.domain.models.AccountModel
import com.example.ca_mybank_clone.domain.models.OperationModel
import com.example.ca_mybank_clone.ui.accountdetails.components.AccountOperationList
import com.example.ca_mybank_clone.ui.theme.Dimensions.smallPadding
import com.example.ca_mybank_clone.ui.theme.backButtonTintColor
import com.example.ca_mybank_clone.ui.theme.titleBackgroundColor


@Composable
fun AccountDetailsScreen(account: AccountModel, goBackToAccountsList: () -> Unit) {
    Column(
        Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.spacedBy(16.dp)

    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.20f)
                .background(titleBackgroundColor),
        ) {
            ToolBar(Modifier.padding(top = 20.dp, bottom = 10.dp), goBackToAccountsList)
            AccountInfoHeader(
                modifier = Modifier
                    .align(Alignment.CenterHorizontally),
                account.holder, account.balance.toString(),

                )
        }

        AccountOperationList(
            account.operations.sortedWith(compareByDescending<OperationModel> { it.date }.thenBy { it.title })
        )

    }
}

@Composable
fun ToolBar(modifier: Modifier, navigationAction: () -> Unit) {
    Row(modifier = modifier.noRippleClickable {
        navigationAction()
    }, horizontalArrangement = Arrangement.spacedBy(smallPadding)) {
        Icon(
            imageVector = Icons.Default.KeyboardArrowLeft,
            contentDescription = "arrow left",
            tint = backButtonTintColor
        )
        Text(
            text = stringResource(id = R.string.mes_comptes),
            color = backButtonTintColor
        )
    }
}

@Composable
fun AccountInfoHeader(modifier: Modifier, title: String, balance: String) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(25.dp)
    ) {
        Text(
            text = stringResource(id = R.string.balanceInquiry, balance),
            style = MaterialTheme.typography.headlineMedium
        )

        Text(
            text = title,
            style = MaterialTheme.typography.headlineSmall
        )
    }
}



