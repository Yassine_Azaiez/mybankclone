package com.example.ca_mybank_clone.ui.accountsList

import androidx.compose.animation.*
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavGraphBuilder
import com.example.ca_mybank_clone.domain.models.AccountModel
import com.example.ca_mybank_clone.ui.screenDestination.bottomNavigation.Screen
import com.google.accompanist.navigation.animation.composable


@OptIn(ExperimentalAnimationApi::class)
fun NavGraphBuilder.accountsListRoute(modifier: Modifier, onAccountClicked : (AccountModel) -> Unit) {

    composable(
        Screen.AccountsList.route,
        exitTransition = {
            slideOutHorizontally(
                targetOffsetX = { -300 },
                animationSpec = tween(
                    300,
                    easing = FastOutSlowInEasing
                )
            ) + fadeOut(animationSpec = tween(200))
        },
        popEnterTransition = {
            slideInHorizontally(
                initialOffsetX = { -300 },
                animationSpec = tween(
                    300,
                    easing = FastOutSlowInEasing
                )
            ) + fadeIn(animationSpec = tween(200))
        },

    ) {
        val viewModel = hiltViewModel<AccountListViewModel>()
        AccountListScreen(modifier,viewModel = viewModel,onAccountClicked)
        }
}