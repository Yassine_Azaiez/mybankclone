package com.example.ca_mybank_clone.ui.screenDestination.bottomNavigation

import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.ui.graphics.vector.ImageVector
import com.example.ca_mybank_clone.R

data class BottomNavigationItem(
    val itemRoute :String = "",
    val itemIcon: ImageVector,
    @StringRes val text: Int
)

val bottomNavigationBarItemsList = listOf(
    BottomNavigationItem( itemRoute = Screen.AccountsList.route, itemIcon = Icons.Default.Star, text = R.string.mes_comptes),
    BottomNavigationItem(itemIcon = Icons.Default.Star, text = R.string.simulation),
    BottomNavigationItem(itemIcon = Icons.Default.Star, text = R.string.a_vous_de_jouer),
)
