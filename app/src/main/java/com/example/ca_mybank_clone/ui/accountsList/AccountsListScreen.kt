package com.example.ca_mybank_clone.ui.accountsList

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.example.ca_mybank_clone.domain.models.AccountModel
import com.example.ca_mybank_clone.ui.accountsList.components.BankAccountSection

@Composable
fun AccountListScreen(
    modifier: Modifier,
    viewModel: AccountListViewModel,
    onAccountClicked : (AccountModel) -> Unit,
) {

   val bankList by viewModel.accountList.collectAsStateWithLifecycle()
    BankAccountSection(modifier,bankList, onAccountClicked)
}