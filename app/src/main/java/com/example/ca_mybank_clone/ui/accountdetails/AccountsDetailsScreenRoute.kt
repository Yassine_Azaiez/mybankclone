package com.example.ca_mybank_clone.ui.accountdetails

import androidx.compose.animation.*
import androidx.navigation.NavGraphBuilder
import com.example.ca_mybank_clone.domain.models.AccountModel
import com.example.ca_mybank_clone.ui.screenDestination.bottomNavigation.Screen
import com.google.accompanist.navigation.animation.composable


@OptIn(ExperimentalAnimationApi::class)
fun NavGraphBuilder.accountsDetailsRoute(account: AccountModel, goBackToAccountsList: () -> Unit) {

    composable(
        Screen.AccountDetails.route,

    ) {

        AccountDetailsScreen(account, goBackToAccountsList)
        }
}