package com.example.ca_mybank_clone.ui.screenDestination.bottomNavigation

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.ca_mybank_clone.ui.theme.CA_MyBank_CloneTheme
import com.example.ca_mybank_clone.ui.theme.Dimensions.BottomBar.BottomNavHeight
import com.example.ca_mybank_clone.ui.theme.Dimensions.BottomBar.iconSize
import com.example.ca_mybank_clone.ui.theme.Dimensions.extraSmallPadding

@Composable
fun NewsBottomNavigation(
    items: List<BottomNavigationItem>,
    selectedItem: Int = 0,

    ) {
    NavigationBar(
        modifier = Modifier
            .fillMaxWidth()
            .height(BottomNavHeight),
        containerColor = MaterialTheme.colorScheme.background,
    ) {
        items.forEachIndexed { index, item ->
            NavigationBarItem(
                selected = index == selectedItem,
                onClick = { },
                icon = {
                    Column(horizontalAlignment = CenterHorizontally) {
                        Icon(
                            painter = rememberVectorPainter(item.itemIcon),
                            contentDescription = null,
                            modifier = Modifier.size(iconSize),
                        )
                        Spacer(modifier = Modifier.height(extraSmallPadding))
                        Text(
                            text = stringResource(id = item.text),
                            style = MaterialTheme.typography.labelSmall
                        )
                    }
                },
            )
        }
    }
}


@Preview
@Preview(uiMode = UI_MODE_NIGHT_YES)
@Composable
fun BottomNavigationPreview() {
    CA_MyBank_CloneTheme(dynamicColor = false) {
        NewsBottomNavigation(items = bottomNavigationBarItemsList, selectedItem = 0)
    }
}