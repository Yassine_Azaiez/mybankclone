package com.example.ca_mybank_clone.ui.accountsList.components

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateColor
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.KeyboardArrowUp
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.ca_mybank_clone.R
import com.example.ca_mybank_clone.core.extenssions.noRippleClickable
import com.example.ca_mybank_clone.domain.models.AccountModel
import com.example.ca_mybank_clone.ui.theme.Dimensions.largePadding
import com.example.ca_mybank_clone.ui.theme.Dimensions.paddingValues

@Composable
fun ExpandableItem(
    modifier: Modifier = Modifier,
    itemList: List<AccountModel>,
    headerText: String,
    onAccountClicked: (AccountModel) -> Unit
) {

    // Expanded state
    var checked by rememberSaveable { mutableStateOf(false) }
    val expanded = updateTransition(targetState = checked, label = "Card")

    val headerIconColor by expanded.animateColor(label = "Card header enabled icon color") { state ->
        if (state) MaterialTheme.colorScheme.onPrimaryContainer else MaterialTheme.colorScheme.primary
    }
    val headerIconRotation by expanded.animateFloat(label = "Icon") { state ->
        if (state) 180f else 0f
    }
    val contentAlpha by expanded.animateFloat(label = "Content alpha") { state ->
        if (state) 1f else 0f
    }

    Card(
        shape = RoundedCornerShape(0.dp),
        colors = CardDefaults.cardColors(containerColor = Color.Transparent),
        modifier = Modifier
        //.scale(cardScale)

    ) {
        Column(
            Modifier
                .fillMaxWidth()
        ) {
            Surface(

                color = Color.Transparent,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(40.dp)
                    .background(Color.Transparent)
                    .padding(top = paddingValues / 2)
                    .padding(bottom = paddingValues / 2)
                    .noRippleClickable { checked = !checked },
            ) {
                Row(
                    modifier = modifier
                        .fillMaxWidth()
                        .padding(horizontal = paddingValues),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = headerText,
                        modifier = Modifier.weight(1f),
                        color = Color.Black
                    )
                    Icon(
                        imageVector = Icons.Outlined.KeyboardArrowUp,
                        contentDescription = "arrow",
                        modifier = Modifier.rotate(headerIconRotation),
                        tint = headerIconColor
                    )
                }
            }
            AnimatedVisibility(checked) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .alpha(contentAlpha)
                ) {
                    Spacer(modifier = Modifier.size(paddingValues / 2))
                    itemList.forEach { account ->
                        ExpandableItemContent(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(
                                    start = largePadding,
                                    end = paddingValues,
                                    top = 10.dp,
                                    bottom = 10.dp
                                ),
                            account.holder,
                            stringResource(id = R.string.balanceInquiry, account.balance)

                        ) {
                            onAccountClicked(account)
                        }
                    }

                }
            }
        }
    }
}