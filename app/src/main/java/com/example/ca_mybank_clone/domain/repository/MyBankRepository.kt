package com.example.ca_mybank_clone.domain.repository

import com.example.ca_mybank_clone.core.networkUtils.ApiResult
import com.example.ca_mybank_clone.domain.models.BankModel
import kotlinx.coroutines.flow.Flow

interface MyBankRepository {
    fun getAccountsLIst() : Flow<ApiResult<List<BankModel>>>
}