package com.example.ca_mybank_clone.domain.models

import com.example.ca_mybank_clone.data.response.AccountType

data class BankModel(
    val name: String,
    val isCaAccount: Boolean,
    val accounts: List<AccountModel>,
    val type: AccountType,
)
