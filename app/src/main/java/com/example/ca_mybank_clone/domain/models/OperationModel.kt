package com.example.ca_mybank_clone.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OperationModel(
    val id: String,
    val title: String,
    val amount: String,
    val date: String,
) : Parcelable
