package com.example.ca_mybank_clone.domain.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AccountModel(
    val holder: String,
    val balance: Double,
    val operations: List<OperationModel>,
) : Parcelable
