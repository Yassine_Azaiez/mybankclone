package com.example.ca_mybank_clone.domain.mapper

import com.example.ca_mybank_clone.core.extenssions.Empty
import com.example.ca_mybank_clone.core.extenssions.timestampToDate
import com.example.ca_mybank_clone.data.response.Account
import com.example.ca_mybank_clone.data.response.AccountType
import com.example.ca_mybank_clone.data.response.Bank
import com.example.ca_mybank_clone.data.response.Operation
import com.example.ca_mybank_clone.domain.models.AccountModel
import com.example.ca_mybank_clone.domain.models.BankModel
import com.example.ca_mybank_clone.domain.models.OperationModel


fun Bank.toBankModel() = BankModel(
      name = name?:String.Empty,
      isCaAccount = (isCa?:0) == 1,
      type = if(isCa == 1) AccountType.CA else AccountType.OTHER,
      accounts = accounts?.map { it.toAccountModel() } ?: emptyList()

)

fun Account.toAccountModel() = AccountModel(
      holder = holder?:String.Empty,
      balance = balance?: 0.0,
      operations = operations?.map {  it.toOperationModel()}?: emptyList()
)


fun Operation.toOperationModel() = OperationModel(
      id = id?:String.Empty,
      title = title?:String.Empty,
      amount= amount?:String.Empty,
      date = (date?:String.Empty).timestampToDate()
)