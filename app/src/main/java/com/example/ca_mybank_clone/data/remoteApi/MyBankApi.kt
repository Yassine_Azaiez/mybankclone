package com.example.ca_mybank_clone.data.remoteApi

import com.example.ca_mybank_clone.core.GET_ACCOUNTS_LIST
import com.example.ca_mybank_clone.data.response.Bank
import retrofit2.Response
import retrofit2.http.GET

interface MyBankApi {

    @GET(GET_ACCOUNTS_LIST)
    suspend fun getAccountsList() : Response<List<Bank>?>
}