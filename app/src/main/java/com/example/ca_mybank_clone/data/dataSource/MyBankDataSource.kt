package com.example.ca_mybank_clone.data.dataSource

import com.example.ca_mybank_clone.core.networkUtils.ApiResult
import com.example.ca_mybank_clone.data.response.Bank


interface MyBankDataSource {

    suspend fun getAccountsList() : ApiResult<List<Bank>?>
}