package com.example.ca_mybank_clone.data.response


data class Operation(
    val id: String?,
    val title: String?,
    val amount: String?,
    val category: String?,
    val date: String?,
)
