package com.example.ca_mybank_clone.data.repository

import com.example.ca_mybank_clone.core.extenssions.onFlowStarts
import com.example.ca_mybank_clone.core.networkUtils.ApiResult
import com.example.ca_mybank_clone.data.dataSource.MyBankDataSource
import com.example.ca_mybank_clone.domain.mapper.toBankModel
import com.example.ca_mybank_clone.domain.models.BankModel
import com.example.ca_mybank_clone.domain.repository.MyBankRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class MyBankRepositoryImp(
    private val remoteSource: MyBankDataSource,
    private val dispatcher: CoroutineDispatcher
) : MyBankRepository {
    override fun getAccountsLIst(): Flow<ApiResult<List<BankModel>>> = flow {
        remoteSource.getAccountsList().run {
            when (this) {
                is ApiResult.Success ->
                    emit(ApiResult.Success( data?.map { it.toBankModel() }?: emptyList()))
                is ApiResult.Error.ServerError -> emit(this)
                else ->  emit(ApiResult.Loading)

            }
        }
    }
    .flowOn(dispatcher)
    .onFlowStarts()


}