package com.example.ca_mybank_clone.data.dataSource

import com.example.ca_mybank_clone.core.networkUtils.ApiResult
import com.example.ca_mybank_clone.core.networkUtils.ApiResultMapper.toApiResult
import com.example.ca_mybank_clone.data.remoteApi.MyBankApi
import com.example.ca_mybank_clone.data.response.Bank


class MyBankDataSourceImp(
    private val api: MyBankApi,
) : MyBankDataSource {
    override suspend fun getAccountsList(): ApiResult<List<Bank>?> =

          toApiResult {
              api.getAccountsList()
          }




}