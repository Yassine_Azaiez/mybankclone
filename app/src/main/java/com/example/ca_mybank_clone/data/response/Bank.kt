package com.example.ca_mybank_clone.data.response

import com.google.gson.annotations.SerializedName

data class Bank(
    val name: String?,
    @SerializedName("isCA")
    val isCa: Int?,
    val accounts: List<Account>?,
)

enum class AccountType(val typeName : String) {
    CA("Credit Agricole") , OTHER("Autres Banques")
}
