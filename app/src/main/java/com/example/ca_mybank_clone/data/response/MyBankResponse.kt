package com.example.ca_mybank_clone.data.response

data class MyBankResponse(
    val bank : List<Bank>?
)
