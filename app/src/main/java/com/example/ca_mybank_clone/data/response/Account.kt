package com.example.ca_mybank_clone.data.response

import com.google.gson.annotations.SerializedName

data class Account(
    val order: Long?,
    val id: String?,
    val holder: String?,
    val role: Long?,
    @SerializedName("contract_number")
    val contractNumber: String?,
    val label: String?,
    @SerializedName("product_code")
    val productCode: String?,
    val balance: Double?,
    val operations: List<Operation>?,
)
