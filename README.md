# mybank

A simple android application developed using Kotlin  based on MVVM clean architecture and some of android jetpack libraries
this App is using [this API](https://cdf-test-mobile-default-rtdb.europe-west1.firebasedatabase.app/banks.json).

## Used Libraries

* [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) : store and manage UI-related data in a lifecycle conscious way.
* [Flow](https://developer.android.com/kotlin/flow) :  flow is a type that can emit multiple values sequentially.
* [Hilt](https://developer.android.com/training/dependency-injection/hilt-android) : Hilt is a dependency injection library for Android.
* [Retrofit](https://square.github.io/retrofit/) :  REST Client library . .
* [coroutines](https://square.github.io/retrofit/) : execution of async (background) tasks.
* [StateFlow](https://developer.android.com/kotlin/flow/stateflow-and-sharedflow) : StateFlow and SharedFlow are Flow APIs that enable flows to optimally emit state updates and emit values to multiple consumers.
* UI written in [Jetpack Compose](https://developer.android.com/jetpack/compose) 


## Features
This application will contain 2 screens:
* Account List Screen
* Account Details Screen

# note 
Dark mode is not well handled and it's work in progress it better to open the app on light mode
