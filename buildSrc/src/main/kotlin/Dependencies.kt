/**
 * To define plugins
 */


/**
 * To define dependencies
 */
object Deps {


    val coreKtx by lazy { "androidx.core:core-ktx:${Versions.corektx}" }
    val lifecycle by lazy { "androidx.lifecycle:lifecycle-runtime-compose:" }
    val composeLifecycle by lazy { "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.composeLifecycle}" }
    val compose by lazy { "androidx.activity:activity-compose:${Versions.compose}" }
    val composeUi by lazy { "androidx.compose.ui:ui" }
    val composeGraphics by lazy { "androidx.compose.ui:ui-graphics" }
    val toolingPreview by lazy { "androidx.compose.ui:ui-tooling-preview" }
    val bom by lazy { "androidx.compose:compose-bom:2023.03.00" }
    val bomInstrumental by lazy { "androidx.compose:compose-bom:2023.03.00" }
    val material3 by lazy { "androidx.compose.material3:material3" }
    val junit by lazy { "junit:junit:${Versions.jUnit}" }
    val junitInstrumental by lazy { "androidx.test.ext:junit:${Versions.jUnitInst}" }
    val junitComposeUi by lazy { "androidx.compose.ui:ui-test-junit4" }
    val espresso by lazy { "androidx.test.espresso:espresso-core:${Versions.espresso}" }
    val uiTooling by lazy { "androidx.compose.ui:ui-tooling" }
    val uiTestManifest by lazy { "androidx.compose.ui:ui-test-manifest" }
    val coroutines by lazy { "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}" }
    val coroutinesTest by lazy { "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutines}" }
    val navigation by lazy { "androidx.navigation:navigation-compose:${Versions.navigation}" }
    val navigationAnnimation by lazy { "com.google.accompanist:accompanist-navigation-animation:${Versions.navigationAnnimation}" }





    val hilt by lazy { "com.google.dagger:hilt-android:${Versions.hilt}" }
    val hiltNavigation by lazy { "androidx.hilt:hilt-navigation-compose:${Versions.hiltNavigation}" }
    val hiltCompiler by lazy { "com.google.dagger:hilt-android-compiler:${Versions.hilt}" }
    val retrofit by lazy { "com.squareup.retrofit2:retrofit:${Versions.retrofit}" }
    val gson by lazy { "com.squareup.retrofit2:converter-gson:${Versions.retrofit}" }
    val logginfInterceptor by lazy { "com.squareup.okhttp3:logging-interceptor:${Versions.loggingInterceptor}" }

    val truth by lazy { "com.google.truth:truth:${Versions.truth}" }
    val mockServer by lazy { "com.squareup.okhttp3:mockwebserver:${Versions.mockServer}" }
    val mockk by lazy { "io.mockk:mockk:${Versions.mockk}" }


}
