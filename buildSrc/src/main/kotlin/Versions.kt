object Versions {


    const val corektx = "1.9.0"
    const val lifecycle = "2.6.1"
    const val composeLifecycle = "2.6.0-rc01"
    const val compose = "1.7.2"
    const val jUnit = "4.13.2"
    const val espresso = "3.5.1"
    const val androidxCore = "1.9.0"
    const val jUnitInst = "1.1.5"
    const val coroutines = "1.3.9"
    const val hilt = "2.44"
    const val hiltNavigation = "1.0.0-alpha03"
    const val retrofit = "2.9.0"
    const val loggingInterceptor = "4.10.0"
    const val truth = "1.1.3"
    const val mockServer = "4.10.0"
    const val mockk = "1.13.2"
    const val navigation = "2.5.3"
    const val navigationAnnimation = "0.31.1-alpha"


}